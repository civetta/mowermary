package com.mowitnow.mowermary.core;

/**
 * This exception is meant to be used when a lawn mower is not correctly deployed on a lawn, i.e. when some of its
 * initialization parameters are missing or invalid.
 * @author Simone Civetta
 *
 */
public class LawnMowerUnsafelyDeployedException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4060295951764326780L;
	
}
