package com.mowitnow.mowermary.core;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;

import com.mowitnow.mowermary.model.Lawn;
import com.mowitnow.mowermary.model.LawnMower;
import com.mowitnow.mowermary.model.Orientation;

/**
 * This class provides parsing and interpretation of the LawnMower instructions.
 * Program builds a mower with the specified width and height, along with all
 * the required lawn mowers.
 * 
 * @author Simone Civetta
 *
 */
public class Program {
	
	private int textRow;
	private int textColumn;
	private Lawn currentLawn;
	private LawnMower currentLawnMower;
	
	private Program() {
		this.currentLawn = new Lawn();
	}
	
	/**
	 * Launches the parsing and interpretation process which allows the creation and the displacement of the lawn
	 * mowers.
	 * @param reader A buffered reader pointing to a source containing the instructions to be read.
	 * @throws LawnMowerUnsafelyDeployedException 
	 */
	public static Program readAndExecuteInstructions(BufferedReader reader) throws LawnMowerUnsafelyDeployedException {
		return new Program().executeWithReader(reader);
	}
	
	/**
	 * Launches the parsing and interpretation process which allows the creation and the displacement of the lawn
	 * mowers.
	 * @param instructions A string containing the instructions to be read. The reading process
	 * is performed by a StringReader.
	 * @throws LawnMowerUnsafelyDeployedException 
	 */
	public static Program readAndExecuteInstructions(String instructions) throws LawnMowerUnsafelyDeployedException {
		BufferedReader bufferedReader = new BufferedReader(new StringReader(instructions));
		return new Program().executeWithReader(bufferedReader);
	}
	
	/**
	 * Parses and interprets the instruction sequence.
	 * The instructions are read character-by-character.
	 * The following assumptions are used:
	 * - The first line of the instructions always contains the lawn size
	 * - Any odd line of the instructions always contains the initial position/orientation of a lawn mower
	 * - Any even line (greater than 0) always contains the moving commands.
	 * 
	 * No error is generated in case of an invalid lawn. If this happens, the lawn is considered to have a 1x1 size,
	 * i.e. only one cell having coordinates (0, 0).
	 * 
	 * In case of an invalid initialization of the lawn mower, an exception is caught and the lawn mower is removed
	 * from its lawn.
	 * 
	 * @param reader A buffered reader containing the instructions to be read.
	 */
	private Program executeWithReader(BufferedReader reader) throws LawnMowerUnsafelyDeployedException {
		
		int data;
		try {
			data = reader.read();
			while(data != -1) {
				
				char command = (char)data;
				
				if (this.textRow == 0) { // Size of the lawn
					readLawnSize(command);
					
				} else {
					
					if (this.textRow % 2 == 1) { // Initial position of the lawnmower
						
						if (this.textColumn == 0) {
							
							if (this.currentLawnMower != null) {
								this.currentLawnMower.shout();
							}
							
							this.currentLawnMower = new LawnMower();
							this.currentLawnMower.setLawn(this.currentLawn);
							this.currentLawn.deployLawnMower(this.currentLawnMower);
						}

						this.readLawnMowerInitialPosition(command);
							
						/*
						}  catch (LawnMowerUnsafelyDeployedException lmude) {
							this.currentLawn.undeployLawnMower(this.currentLawnMower);
							this.currentLawnMower = null;
						}
						*/

					} else { // Actions of the lawnmower
						this.moveLawnMower(command);
					}
				}

				if (command == '\n') {
					this.textRow++;
					this.textColumn = 0;
				} else {
					this.textColumn++;
				}
				
				data = reader.read();
			}
			
			if (this.currentLawnMower != null) {
				this.currentLawnMower.shout();
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return this;
	}	
	
	/**
	 * Reads and sets the lawn size.
	 * The following assumptions are used:
	 * - Maximum width and height of the lawn is 9 (thus generating a side of 10 cells).
	 * - As a consequence, the characters at positions 0 and 2 are meant to contain width and height of the lawn,
	 *   respectively.
	 *   
	 * @param command A single character containing the size information.
	 */
	private void readLawnSize(char command) {
		int number = Character.getNumericValue(command);
		if (this.textColumn == 0 && number >= 0 && number <= 9) {
			this.currentLawn.setWidth(number);
			return;
		}

		if (this.textColumn == 2 && number >= 0 && number <= 9) {
			this.currentLawn.setHeight(number);
			return;
		}
	}
	
	/**
	 * Reads and sets the initial position and orientation of the current lawn mower.
	 * If any of the initialization variables is missing or invalid a LawnMowerUnsafelyDeployedException is thrown.
	 * The following assumptions are used:
	 * - Maximum x or y position is 9.
	 * - As a consequence, the characters at positions 0, 2 and 4 are meant to contain X, Y and orientation of the
	 *   lawn mower, respectively.
	 * 
	 * @param command A single character containing the initialization information.
	 * @throws LawnMowerUnsafelyDeployedException if any of the initialization variables is missing or invalid.
	 */
	private void readLawnMowerInitialPosition(char command) throws LawnMowerUnsafelyDeployedException {
		if (this.currentLawnMower == null)
			return;
		
		int number = Character.getNumericValue(command);
		
		switch (this.textColumn) {
			case 0: // First digit (X)
				if (number >= 0 && number <= 9) {
					this.currentLawnMower.setX(number);
					return;
				}				
				throw new LawnMowerUnsafelyDeployedException();
				
			case 1: // If newline found, throw an exception
				if (command != '\n') {
					break;
				}
				throw new LawnMowerUnsafelyDeployedException();
				
			case 2: // Second digit (Y)
				if (number >= 0 && number <= 9 && this.currentLawn.canOccupyPosition(this.currentLawnMower.getX(), number)) {
					this.currentLawnMower.setY(number);
					return;
				}				
				throw new LawnMowerUnsafelyDeployedException();
				
			case 3: // If newline found, throw an exception
				if (command != '\n') {
					break;
				}
				throw new LawnMowerUnsafelyDeployedException();
				
			case 4: // Third digit (Orientation)
				Orientation orientation = Orientation.valueOf(command);
				if (orientation != Orientation.UNKNOWN) {
					this.currentLawnMower.setOrientation(Orientation.valueOf(command));
					return;
				}
				
				throw new LawnMowerUnsafelyDeployedException(); 
		}	
	}
	
	/**
	 * Performs the displacement of the currently active lawn mower.
	 * @param command A single character containing the direction information.
	 */
	private void moveLawnMower(char command) {
		
		if (this.currentLawnMower == null)
			return;
		
		switch (command) {
			case 'G':
				this.currentLawnMower.rotateLeft();
				break;
	
			case 'A':
				this.currentLawnMower.advance();
				break;
	
			case 'D':
				this.currentLawnMower.rotateRight();
				break;
		}
	}
	
	/**
	 * Retrieves the current lawn.
	 * @return The lawn for the current program.
	 */
	public Lawn getCurrentLawn() {
		return this.currentLawn;
	}
}
