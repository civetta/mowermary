package com.mowitnow.mowermary.core;

public class Utils {
	public static boolean isNumeric(String str) {
		
		try {  
			Double.parseDouble(str);  
		} catch(NumberFormatException nfe) {  
			return false;  
		}
		
		return true;  
	}
}
