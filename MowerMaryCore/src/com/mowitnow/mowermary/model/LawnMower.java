package com.mowitnow.mowermary.model;

public class LawnMower {
	
	private Lawn lawn;
	private int x;
	private int y;
	private Orientation orientation;
	
	public LawnMower() {
		this.orientation = Orientation.UNKNOWN;
	}
	
	public Lawn getLawn() {
		return lawn;
	}

	public void setLawn(Lawn lawn) {
		this.lawn = lawn;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public Orientation getOrientation() {
		return orientation;
	}

	public void setOrientation(Orientation orientation) {
		this.orientation = orientation;
	}
	
	public void rotateLeft() {
		switch(this.orientation) {
			case NORTH:
				this.orientation = Orientation.WEST;
				break;
				
			case WEST:
				this.orientation = Orientation.SOUTH;
				break;
				
			case SOUTH:
				this.orientation = Orientation.EAST;
				break;
			
			case EAST:
				this.orientation = Orientation.NORTH;
				break;
				
			default:
				this.orientation = Orientation.UNKNOWN;
				break;			
		}
	}
	
	public void rotateRight() {
		switch(this.orientation) {
			case NORTH:
				this.orientation = Orientation.EAST;
				break;
				
			case WEST:
				this.orientation = Orientation.NORTH;
				break;
				
			case SOUTH:
				this.orientation = Orientation.WEST;
				break;
			
			case EAST:
				this.orientation = Orientation.SOUTH;
				break;
				
			default:
				this.orientation = Orientation.UNKNOWN;
				break;			
		}		
	}
	
	public void advance() {
		int newX = this.x;
		int newY = this.y;
		
		switch(this.orientation) {
			case NORTH:
				newY++;
				break;
				
			case WEST:
				newX--;
				break;
				
			case SOUTH:
				newY--;
				break;
			
			case EAST:
				newX++;
				break;
				
			default:
				return;
		}
		
		if (this.lawn.canOccupyPosition(newX, newY)) {
			this.x = newX;
			this.y = newY;
		}
	}
	
	public void shout() {
		System.out.println(this.toString());
	}
	
	public String toString() {
		return Integer.toString(x) + " " + Integer.toString(y) + " " + String.valueOf(this.orientation.getValue());
	}
}
