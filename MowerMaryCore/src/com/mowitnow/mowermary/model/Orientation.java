package com.mowitnow.mowermary.model;

/**
 * A simple class containing the definition of the orientation and some conveniency methods.
 * @author Simone Civetta
 *
 */
public enum Orientation {
	UNKNOWN('U'),
	NORTH('N'), 
	SOUTH('S'), 
	WEST('W'), 
	EAST('E');
	
	private char value;

    private Orientation(char value) {
            this.value = value;
    }
    
    public static Orientation valueOf(char c) {
    	switch (c) {
			case 'N':
				return NORTH;
				
			case 'S':
				return SOUTH;
				
			case 'W':
				return WEST;
				
			case 'E':
				return EAST;
	
			default:
				return UNKNOWN;
		}
    }

	public char getValue() {
		return value;
	}

}
