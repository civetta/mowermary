package com.mowitnow.mowermary.model;

import java.util.ArrayList;

/**
 * The lawn inside which the lawn mowers are deployed.
 * @author Simone Civetta
 *
 */

public class Lawn {
	private int width;
	private int height;
	private ArrayList<LawnMower> lawnMowers;
	
	public Lawn() {
		this.lawnMowers = new ArrayList<LawnMower>();
	}
	
	/**
	 * Gets the width of the grid of the lawn.
	 * @return Width of the lawn.
	 */	
	public int getWidth() {
		return width;
	}
	
	/**
	 * Sets the width of the grid of the lawn.
	 * @param width Width of the lawn.
	 */
	public void setWidth(int width) {
		this.width = width;
	}

	/**
	 * Gets the height of the grid of the lawn.
	 * @return Height of the lawn.
	 */
	public int getHeight() {
		return height;
	}

	/**
	 * Sets the height of the grid of the lawn.
	 * @param height Height of the lawn.
	 */
	public void setHeight(int height) {
		this.height = height;
	}

	/**
	 * Deploys (adds) a lawn mower into the lawn.
	 * @param lawnMower The lawn mower to be added.
	 */
	public void deployLawnMower(LawnMower lawnMower) {
		this.lawnMowers.add(lawnMower);
	}
	
	/**
	 * Undeploys (removes) a lawn mower from the lawn.
	 * @param lawnMower The lawn mower to be removed.
	 */
	public void undeployLawnMower(LawnMower lawnMower) {
		this.lawnMowers.remove(lawnMower);
	}

	/**
	 * Bulkly sets all the lawn mowers of the lawn.
	 * @param lawnMowers An ArrayList of LawnMowers.
	 */
	public void setLawnMowers(ArrayList<LawnMower> lawnMowers) {
		this.lawnMowers = lawnMowers;
	}
	
	/**
	 * Ask all the deployed lawn mowers to print their current position.
	 */
	public void callLawnMowers() {
		for (LawnMower lm : this.lawnMowers) {
			lm.shout();
		}
	}
	
	/**
	 * Returns a boolean variable stating if the desired (X, Y) coordinate can be used.
	 * @param x X coordinate of the desired position.
	 * @param y Y coordinate of the desired position.
	 * @return a Boolean stating whether the desired position is free (true) or out-of-bounds/already taken (false).
	 */
	public Boolean canOccupyPosition(int x, int y) {
		if (x < 0 || x > width)
			return false;
		
		if (y < 0 || y > height)
			return false;
		
		for (LawnMower lm : this.lawnMowers) {
			if (lm.getX() == x && lm.getY() == y)
				return false;
		}
		
		return true;
	}
	
	/**
	 * Returns a String displaying the current state of the deployed lawn mowers.
	 * @return a String containing the coordinates of all the current lawn mowers in the following format:
	 * - a line for each lawn mower
	 * - each single line displays X Y and orientation, separated by a space character
	 */
	public String printAllLawnMowers() {
		String positions = "";
		for (LawnMower lm : this.lawnMowers) {
			positions += lm.toString() + "\n";
		}
		
		return positions;
	}
	
	/**
	 * Retrieves the currently deployed lawn mowers.
	 * @return The ArrayList of the currently deployed lawn mowers.
	 */
	public ArrayList<LawnMower> getLawnMowers() {
		return this.lawnMowers;
	}
}
