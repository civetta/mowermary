import static org.junit.Assert.*;

import org.junit.Test;

import com.mowitnow.mowermary.core.LawnMowerUnsafelyDeployedException;
import com.mowitnow.mowermary.core.Program;


public class InstructionExecutionTest {

	@Test
	public void CurrentLawnShouldHaveCorrectHeight() throws LawnMowerUnsafelyDeployedException {
		Program p = Program.readAndExecuteInstructions("5 5\n");
		assertTrue("The lawn has a height exactly equal to 5", p.getCurrentLawn().getHeight() == 5);
	}
	
	@Test(expected=LawnMowerUnsafelyDeployedException.class)
	public void ProgramShouldFailWhenMowerInstructionsAreIncomplete() throws LawnMowerUnsafelyDeployedException {
		Program p = Program.readAndExecuteInstructions("5 5\n1 2\n");
	}

}
