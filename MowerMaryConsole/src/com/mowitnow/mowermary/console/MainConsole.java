package com.mowitnow.mowermary.console;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import com.mowitnow.mowermary.core.Program;

public class MainConsole {

	private final static String FILENAME = "instructions.txt";
	
	public static void main(String[] args) {

		readFileAndExecuteProgram();
	}
	
	private static void readFileAndExecuteProgram() {
		FileInputStream fis;
		try {
			fis = new FileInputStream(FILENAME);
			BufferedReader in = new BufferedReader(new InputStreamReader(fis, "UTF-8"));
			Program p = new Program();
			p.readAndExecuteInstructions(in);
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} 
	}

}
