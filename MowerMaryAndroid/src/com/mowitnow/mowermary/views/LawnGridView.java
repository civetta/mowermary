package com.mowitnow.mowermary.views;

import java.util.ArrayList;

import com.mowitnow.mowermary.model.LawnMower;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

public class LawnGridView extends View {
	
	private static final float MARGIN = 10.0f;
	
	Paint paint = new Paint();
	private int gridWidth;
	private int gridHeight;
	private ArrayList<LawnMower> lawnMowers;

	public LawnGridView(Context context) {
		super(context);
		this.setLawnMowers(new ArrayList<LawnMower>());
	}
	
	public LawnGridView(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.setLawnMowers(new ArrayList<LawnMower>());
	}
	
	public LawnGridView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		this.setLawnMowers(new ArrayList<LawnMower>());
	}

	@Override
    public void onDraw(Canvas canvas) {
        paint.setColor(Color.BLACK);
        paint.setStrokeWidth(0);
        
        // Draw border
        canvas.drawRect(MARGIN - 2, MARGIN - 2, this.getWidth() - MARGIN + 2, this.getHeight() - MARGIN + 2, paint);

        // Draw inner box
        paint.setColor(Color.GREEN);        
        canvas.drawRect(MARGIN, MARGIN, this.getWidth() - MARGIN, this.getHeight() - MARGIN, paint);
            
        paint.setStrokeWidth(2);
        paint.setColor(Color.WHITE);
        
        // Draw vertical grid
        float horizontalOffset = (this.getWidth() - 2 * MARGIN) / (float)(this.getGridWidth() + 1);
        for (int i = 1; i <= this.getGridWidth(); i++) {
        	canvas.drawLine(MARGIN + horizontalOffset * i, MARGIN, MARGIN + horizontalOffset * i, this.getHeight() - MARGIN, paint);
        }
        
        // Draw horizontal grid
        float verticalOffset = (this.getHeight() - 2 * MARGIN) / (float)(this.getGridHeight() + 1);
        for (int i = 1; i <= this.getGridHeight(); i++) {
        	canvas.drawLine(MARGIN, MARGIN + verticalOffset * i, this.getWidth() - MARGIN, MARGIN + verticalOffset * i, paint);
        }     
        
        // Draw all lawnMowers
        paint.setStrokeWidth(2);
        paint.setColor(Color.YELLOW);
        
        for (LawnMower lm : this.getLawnMowers()) {
        	float x = MARGIN + lm.getX() * horizontalOffset;
        	float y = this.getHeight() - MARGIN - (lm.getY() + 1) * verticalOffset;
        	canvas.drawRect(x, y, x + horizontalOffset, y + verticalOffset, paint);
        }
    }

	public int getGridWidth() {
		return gridWidth;
	}

	public void setGridWidth(int width) {
		this.gridWidth = width;
	}

	public int getGridHeight() {
		return gridHeight;
	}

	public void setGridHeight(int height) {
		this.gridHeight = height;
	}

	private ArrayList<LawnMower> getLawnMowers() {
		return lawnMowers;
	}

	public void setLawnMowers(ArrayList<LawnMower> lawnMowers) {
		this.lawnMowers = lawnMowers;
	}

}
