package com.mowitnow.mowermary.activities;
import com.mowitnow.mowermary.R;
import com.mowitnow.mowermary.core.Program;
import com.mowitnow.mowermary.model.Lawn;
import com.mowitnow.mowermary.views.LawnGridView;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.widget.TextView;

public class ResultActivity extends Activity {

	static final String INSTRUCTIONS = "INSTRUCTIONS";
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        
        Bundle bundle = this.getIntent().getExtras();
        String instructions = bundle.getString(INSTRUCTIONS);
        
    	Program p = new Program();   
        p.readAndExecuteInstructions(instructions);
        
        this.printLog(p);
        this.drawGrid(p);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_result, menu);
        return true;
    }
    
    private void printLog(Program program) {
    	TextView instructionEditText = (TextView)findViewById(R.id.resultText);
        instructionEditText.setText(program.getCurrentLawn().printAllLawnMowers());
    }
    
    private void drawGrid(Program program) {
    	
    	Lawn lawn = program.getCurrentLawn();
    	
    	LawnGridView lawnGrid = (LawnGridView) findViewById(R.id.lawnGrid);
        lawnGrid.setGridHeight(lawn.getHeight());
        lawnGrid.setGridWidth(lawn.getWidth());
        lawnGrid.setLawnMowers(lawn.getLawnMowers());
        
        lawnGrid.invalidate();
    }
}
